\documentclass[12pt, twoside]{amsart}

\newcommand{\R}{{\mathbb R}}
\newcommand{\Z}{{\mathbb Z}}
\newcommand{\C}{{\mathbb C}}
\newcommand{\xn}{{\vec x_n}}
\newcommand{\bigH}{{\mathbb H}}
\newcommand{\Rbar}{\overline{\R}}

\setlength{\textwidth}{6.5in}
\setlength{\textheight}{8.5in}
\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\topmargin}{0in}

\everymath{\displaystyle}
\usepackage{commath}


\begin{document}


\centerline{\large \sc Topics in Geometry - Homework 5}
\centerline{\large  Sophie Stephenson \ November 4, 2019}
\bigskip

\begin{enumerate}


\item[1.)]  Let $S^1$ be the unit circle in $\R^2$ and let $C$ be the circle $(x - h)^2 + (y -k)^2 = R^2,$ $R > 1$. Show that $S^1$ is perpendicular to $C$ if and only if $R = \sqrt{h^2 + k^2 -1}$. Instead of computing tangent lines, here's another way to approach the problem. Let $P$ be one of the points of intersection of the two circles (if there's just one point, then they are tangent, not perpendicular to each other.) Consider the triangle $T$ with vertices (0,0), $P$, and $(h, k)$. The circles are perpendicular at $P$ if and only if the interior angle of $T$ at the vertex $P$ is $\pi/2$, which is true if and only if the Pythagorean Theorem holds for $T$. Use the Pythagorean Theorem to derive the formula $R = \sqrt{h^2 + k^2 - 1}$. \\
\begin{proof} We will show that $S^1$ is perpendicular to $C \iff R = \sqrt{h^2 + k^2 -1}$. \\

\noindent $(\implies)$ Assume $S^1$ is perpendicular to $C$. As stated above, this implies that in triangle $T$, the vertex $P$ is $\pi/2$ and the Pythagorean Theorem holds for $T$. Thus, we can derive
	$$\big(d_{Euc}((0, 0), P)\big)^2 + \big(d_{Euc}(P, (h, k))\big)^2 = \big(d_{Euc}((h, k), (0, 0))\big)^2.$$
Since $S^1$ is the unit circle, and $P$ must be on $S^1$, we know that $d_{Euc}((0, 0), P)= 1$. Thus, if $P = (x, y)$, we have 
	$$1 + \Big(\sqrt{(x - h)^2 + (y - k)^2}\Big)^2 = \Big(\sqrt{h^2 + k^2}\Big)^2.$$
Expanding, we find
	$$(x - h)^2 + (y - k)^2 = h^2 + k^2 - 1.$$
Thus, $R^2 = h^2 + k^2 -1$, and it follows that $R = \sqrt{h^2 + k^2 -1}$, as desired.\\

\noindent $(\impliedby)$ Now, assume that $R = \sqrt{h^2 + k^2 -1}$ and $R > 0$. Then, $T$ is a triangle with sides equal to $1$, $\sqrt{h^2 + k^2 -1}$, and $\sqrt{h^2 + k^2}$. We can then use the law of cosines to write
$$\Big(\sqrt{h^2 + k^2}\Big)^2 = h^2 + k^2 = 1 +  h^2 + k^2 -1 - 2(h^2 + k^2 -1) \cos(\theta),$$ 
where $\theta$ is the interior angle at the vertex $P$. Simplifying, we find
$$0 = - 2(h^2 + k^2 -1) \cos(\theta),$$
and since we have that $R > 0$, this implies that $\cos(\theta) = 0$. Thus, $\theta = \pi/2$, and the circles are perpendicular.

\end{proof}
\bigskip

\item[2.)] Let $P$ and $Q$ be distinct points in $\bigH$ such that $Re(P) \not = Re(Q)$. In class we showed there is a unique Euclidean circle with center on the real axis that passes through $P$ and $Q$. Let $c$ be the center of the circle and $R$ the radius. Express $c$ and $R$ in terms of $Re(P), Re(Q), Im(P)$ and $Im(Q)$. 
\begin{proof} First, we need to find the perpendicular bisector of the line between $P$ and $Q$. The slope of the line between $P$ and $Q$ is $\tfrac{Im(Q) - Im(P)}{Re(Q) - Re(P)}$, so the slope of the perpendicular bisector is $-\tfrac{Re(Q) - Re(P)}{Im(Q) - Im(P)}$. We also know that the bisector will pass through the midpoint between $P$ and $Q$, 
$$m = \big(\frac{Re(P) + Re(Q)}{2}, \frac{Im(P) + Im(Q)}{2}\big).$$
Thus, using point slope form, the equation for the perpendicular bisector is
$$y - \frac{Im(P) + Im(Q)}{2} = -\frac{Re(Q) - Re(P)}{Im(Q) - Im(P)}\big(x - \frac{Re(P) + Re(Q)}{2}\big).$$
The center of the circle is the point where the perpendicular bisector intersects the real axis, or where $y = 0$. Thus, we can find $x$ as follows: 
\begin{align*}
	 - \frac{Im(Q) + Im(P)}{2} &= -\frac{Re(Q) - Re(P)}{Im(Q) - Im(P)}\big(x - \frac{Re(P) + Re(Q)}{2}\big)\\ 
	\implies - \frac{Im^2(Q) - Im^2(P)}{2} &= -(Re(Q) - Re(P))x + \frac{Re^2(P) - Re^2(Q)}{2}\\
	\implies x &= \frac{Im^2(Q) - Im^2(P)}{2(Re(Q) - Re(P))}+ \frac{Re(P) + Re(Q)}{2}\\
\end{align*}
This is the real part of $c$, and since $c$ lies on the real axis, it has no imaginary part. Thus, $x = Re(c) = c$, and we see that the center of our circle is the complex coordinate
$$c = \frac{Im^2(Q) - Im^2(P)}{2(Re(Q) - Re(P))}+ \frac{Re(P) + Re(Q)}{2}.$$\\
The radius $R$ is simply the distance between $Q$ and $c$, so \\
$$R = \abs{Q - c} = \sqrt{(Re(Q) - \frac{Im^2(Q) - Im^2(P)}{2(Re(Q) - Re(P))} - \frac{Re(P) + Re(Q)}{2})^2 + Im^2(Q)}.$$
\end{proof}
\bigskip

\newpage

\item[3.)] Give explicit descriptions (equations) of two hyperbolic lines that pass through $i$ and are parallel to the hyperbolic line $Re(z) = 3$. 
\begin{proof}[Solution] One is the $Re(z) = 0$, which is clearly parallel to $Re(z) = 3$ and passes through $i$. Another is the hyperbolic line that passes through $i$ and 1. From problem 2, we know that the center of this circle is 
$$c = -\frac{1}{2} + \frac{1}{2} = 0.$$
This makes sense since $i$ and 1 are equidistant from the origin. It is easy to deduce that the radius of the circle is 1, so the equation of the second hyperbolic line is $|z| = 1.$ \\
\end{proof}
\bigskip

\item[4.)] Give explicit descriptions (equations) of two hyperbolic lines that pass through $i$ and are parallel to the upper half of the Euclidean circle with center -2 on the real axis and radius 1.
\begin{proof}[Solution] Again, $Re(z) = 0$ is clearly parallel to this semicircle and passes through $i$. Another hyperbolic line parallel to this semicircle is the line passing through $i$ and -1. This happens to be the same circle described above: $|z| = 1$. 

\end{proof}
\bigskip

\item[5.)] In class on Wednesday, we'll prove that if $T(z) = \tfrac{az + b}{cz + d}$ is a Mobius transformation that sends $\Rbar$ to $\Rbar$, then either $a, b, c$ and $d$ are real and $ad - bc = 1$, or $a, b, c$ and $d$ are purely imaginary (of the form $iy$) and $ad - bc = 1$. In class, we will assume that $a, c \not = 0$. In this problem, make the similar argument in the case when $a \not = 0$ and $c = 0$. 
\begin{proof} Assume that $T(z) = \tfrac{az + b}{d}$ with $a \not = 0$ and $ad - bc = 1$, and assume $T$ sends $\Rbar$ to $\Rbar$. First, notice that
 $$T(0) = \tfrac{b}{d}, \ \ \ T^{-1}(0) = - \tfrac{b}{a}.$$
Since $T : \Rbar \rightarrow \Rbar$, we know that $\tfrac{b}{d}, -\tfrac{b}{a} \in \Rbar$. In fact, we have that $a \not = 0$ by assumption, and since $ad - bc = ad \not = 0$, surely $d \not = 0$. Thus, $\tfrac{b}{d}, -\tfrac{b}{a} \in \R$. We can also deduce that
$$b = -aT^{-1}(0), \ \ \ d = \frac{b}{T(0)} = \frac{-aT^{-1}(0)}{T(0)}.$$
Thus we can rewrite our transformation $T$ as 
$$T(z) = \frac{az + b}{d} = \frac{az -a T^{-1}(0)}{\tfrac{-a T^{-1}(0)}{T(0)}} = \frac{aT(0)z - aT(0)T^{-1}(0)}{-aT^{-1}(0)}.$$
The determinant of $T$ written in this form is $det(T) = -a^2T(0)T^{-1}(0) = 1$. We have already shown that $T(0), T^{-1}(0) \in \R$, and clearly $1 \in \R$; thus, we must have that $a^2 \in \R$. This requires that $a$ be either a real number or be entirely imaginary. (If $a$ is an imaginary number with a nonzero real part, then $a^2$ necessarily has a nonzero imaginary part.)\\

Now, look at the coefficients of the rewritten $T$. Again, since $T(0), T^{-1}(0) \in \R$, if $a \in \R$, then $b$ and $d$ are in $\R$; if $a$ is completely imaginary, then $b$ and $d$ are completely imaginary. Thus, either $a, b,$ and $d$ are all real or $a, b,$ and $d$ are all purely imaginary, as desired.

\end{proof}
\bigskip

\end{enumerate}


\end{document}